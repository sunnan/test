<?php
/**
 * Created by PhpStorm.
 * User: suitongxian
 * Date: 2019/7/27
 * Time: 10:45 AM
 */

namespace App\Logics\Test;

use App\Models\TestModel;

class SuitongxianLogic
{
    public function test($test)
    {
        //此处进行数据逻辑处理
        $data = TestModel::testSelect($test);

        return $data;
    }
}