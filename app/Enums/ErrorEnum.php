<?php
/**
 * Created by PhpStorm.
 * User: suitongxian
 * Date: 2019/7/27
 * Time: 10:36 AM
 */

namespace App\Enums;

/**
 * Class ErrorEnum
 * @package App\Enums
 * 这里是定义错误信息的地方，不同错误信息分开定义
 */
class ErrorEnum
{
    const ERROR_PARAMS = '100001=>参数错误';
}