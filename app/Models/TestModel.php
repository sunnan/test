<?php
/**
 * Created by PhpStorm.
 * User: suitongxian
 * Date: 2019/7/27
 * Time: 11:03 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TestModel extends Model
{
    //此处可根据需要需要连接不同数据库
    //protected $connection = 'test';

    protected $table = 'admin_users';

    public static function testSelect($data)
    {
        $select = self::where('chapter_id', 49104)->first();
        return $select;
    }
    public static function userSelect($data){
    	$select  = self::where('id',1)->first();
    	return $select;
    }
}
