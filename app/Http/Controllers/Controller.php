<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    //请求成功返回json
    protected function outputJson($data = [])
    {
        $result = [
            'errcode' => 0,
            'errmsg' => 'success',
            'data' => $data
        ];

        return $this->outputRawJson($result);
    }

    //请求失败放回json
    protected function outputErrorJson($err, $errmsg = '')
    {
        $arrErrorMsg = explode('=>', $err);
        $errCode = isset($arrErrorMsg[0]) ? $arrErrorMsg[0] : 1;
        if ($errmsg) {
            $errMsg = $errmsg;
        } else {
            $errMsg = isset($arrErrorMsg[1]) ? $arrErrorMsg[1] : 'Unknown Error!';
        }
        $result = [
            'errcode' => $errCode,
            'errmsg' => $errMsg,
            'data' => []
        ];

        return $this->outputRawJson($result);
    }

    protected function outputRawJson(array $data = [])
    {
        return response()->json($data);
    }

    protected function paramsValidator($validParams)
    {
        $messages = [
            'required' => ' :attribute 参数不能为空',
            'integer'  => ' :attribute 字段必须是整数',
            'array'    => ' :attribute 字段必须是数组',
        ];

        $validator = Validator::make($this->request->all(), $validParams, $messages);

        if ($validator->fails()) {
            return $validator->errors()->first();
        }
        return true;
    }
}
