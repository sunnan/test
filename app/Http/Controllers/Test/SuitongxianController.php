<?php

/**
 * Created by PhpStorm.
 * User: suitongxian
 * Date: 2019/7/27
 * Time: 10:29 AM
 */
namespace App\Http\Controllers\Test;


use App\Http\Controllers\Controller;
use App\Enums\ErrorEnum;
use App\Logics\Test\SuitongxianLogic;

class SuitongxianController extends Controller
{
    public function test(SuitongxianLogic $suitongxianLogic)
    {
        $test = $this->request->input('test');

        //参数校验
        $validParams = [
            'test' => 'required|integer'
        ];
        if (true !== ($paramStatus = $this->paramsValidator($validParams))) {
            return $this->outputErrorJson(ErrorEnum::ERROR_PARAMS, $paramStatus);
        }

        $data = $suitongxianLogic->test($test);

        return $this->outputJson($data);
    }
}