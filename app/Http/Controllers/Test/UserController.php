<?php
/**
 * Created by PhpStorm.
 * User: Sunnan
 * Date: 2019/7/31
 * Time: 13:11
 */


namespace App\Http\Controllers\Test;

use App\Enums\ErrorEnum;
use App\Http\Controllers\Controller;
use App\Logics\Test\UserLogic;
class UserController extends Controller
{
    /**
     * @param id int 参数id
     * @param UserLogic $userLogic
     * @return \Illuminate\Http\JsonResponse
     */
    public function info(UserLogic $userLogic)
    {
        $id = $this->request->input('id');
        //参数校验
        $validParams = [
            'id' => 'required |integer '
        ];
        if (true !== ($paramsStatus = $this->paramsValidator($validParams))) {
            return $this->outputErrorJson(ErrorEnum::ERROR_PARAMS, $paramsStatus);
        }
        $data = $userLogic->info($id);
        return $this->outputJson($data);
    }
    public function all(AboutLogic $aboutLogic)
    {
        $lid = $this->request->input('lid');

        //参数校验
        $validParams = [
            'lid' =>'required | intager'
        ];
        if(true !== ($paramsStatus = $this->paramsValidator($validParams))){
            return $this->outputErrorJson(ErrorEnum::ERROR_PARAMS,$paramsStatus)
        }
        $data =$aboutLogic->info($lid);
        return $this->outputJson($data);
    }
}

