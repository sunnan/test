<?php
/**
 * Created by PhpStorm.
 * User: suitongxian
 * Date: 2019/7/27
 * Time: 3:48 PM
 */

namespace App\Thirds;


class TestThird
{
    protected static $restapi = 'test';

    public static function test($params)
    {
        $ret = static::get(
            '/test',
            $params,
            static::$restapi
        );
        if (!$ret) {
            return [];
        }
        return $ret;
    }
}
