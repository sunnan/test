<?php
/**
 * Created by PhpStorm.
 * User: suitongxian
 * Date: 2019/7/27
 * Time: 3:43 PM
 */

namespace App\Thirds;

use GuzzleHttp\Client;

class BaseThird
{
    protected static function get($api, $params, $module, $headers = [])
    {
        return self::_send($api, $params, $module, $headers, 'GET');
    }

    protected static function post($api, $params, $module, $headers = [])
    {
        return self::_send($api, $params, $module, $headers, 'POST');
    }

    protected static function _send($api, $params, $module, $headers, $method)
    {
        $client = new Client();
        $data = $client->request($method, config('restapi.'.$module.'.domain').$api, ['query'=>$params]);
        $ret       = json_decode($data->getBody()->getContents(), true);
        switch (true) {
            case !$ret:
            case isset($ret['errcode']) && $ret['errcode'] != 0:
            case isset($ret['code']) && $ret['code'] != 0:
            case isset($ret['errno']) && $ret['errno'] != 0:
                return false;
        }

        //多返回兼容
        switch ( true ) {
            case isset($ret['data']):
                return $ret['data'];
            case isset($ret['result']):
                return $ret['result'];
            case isset($ret['user_list']):
                return $ret['user_list'];
            default:
                unset($ret['errcode'], $ret['errmsg']);
                return $ret;
        }
    }
}